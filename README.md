# writeModels

This R package is used to write the output of linear models to xlsx files. Output formats are intended to summarize model results for use as supplemental materials in scientific publications. 

# Installation

To install the most current version, use `devtools::install_gitlab`.

``` 
devtools::install_gitlab('treyjscott/writeModels')
library(writeModels)
```
